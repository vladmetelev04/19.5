﻿#include <iostream>

class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Cat : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Meow!\n";
        std::cout << "\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Woof!\n";
        std::cout << "\n";
    }

};

class Duck : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Quack!\n";
        std::cout << "\n";
    }
};

class Cow : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Moo!\n";
        std::cout << "\n";
    }
};

class Bear : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Grrr!\n";
        std::cout << "\n";
    }
};

class Snake : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Sssss!\n";
        std::cout << "\n";
    }
};

class Frog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Ribbit.\n";
        std::cout << "\n";
        std::cout << "\n";
        std::cout << "Task completed!\n";
        std::cout << "\n";
    }
};



int main()
{
    Animal* animals[7];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Duck();
    animals[3] = new Cow();
    animals[4] = new Bear();
    animals[5] = new Snake();
    animals[6] = new Frog();

    for (Animal* a : animals)
        a->makeSound();
}